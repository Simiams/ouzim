import {RequestParam} from "./types";

export const MonsterForm: RequestParam[] = [
    {name: "name", value: "", type: "text"},
    {name: "description", value: "", type: "text"},
    {name: "url", value: "", type: "text"},
    {name: "world", value: [], type: "autocomplete", possibleValues: []},
    {name: "types", value: [], type: "checkbox", possibleValues: []},
]
export const WorldForm: RequestParam[] = [
    {name: "name", value: "", type: "text"},
]
export const TypeForm: RequestParam[] = [
    {name: "name", value: "", type: "text"},
]
export const FightForm: RequestParam[] = [
    {name: "world", value: "", type: "autocomplete", possibleValues: []},
    {name: "winner", value: "", type: "text"},
    {name: "loser", value: "", type: "text"},
]