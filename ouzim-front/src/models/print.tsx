import {PrintField} from "./types";

export const MonsterPrint: PrintField[] = [
    {name: "name", value: ""},
    {name: "description", value: ""},
    {name: "url", value: ""},
    {name: "world", value: []},
    {name: "types", value: []},
]
export const WorldForm: PrintField[] = [
    {name: "name", value: ""},
]
export const TypeForm: PrintField[] = [
    {name: "name", value: ""},
]
export const FightForm: PrintField[] = [
    {name: "world", value: ""},
    {name: "winner", value: ""},
    {name: "loser", value: ""},
]