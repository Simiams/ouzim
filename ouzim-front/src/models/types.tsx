import {Dayjs} from "dayjs";

export type Monster = {
    id: number,
    name: string,
    description: string,
    url: string,
    world: {
        name: string
    }
    types: {
        name: string
    }[]
}

export type MonsterReq = {
    name: string,
    description: string,
    url: string,
    world: string
    types: string[]
}

export type World = {
    id: number,
    name: string,
}
export type WorldReq = {
    name: string,
}

export type Type = {
    id: number,
    name: string,
}
export type TypeReq = {
    name: string,
}

export type Fight = {
    id: number,
    date: Dayjs,
    loser: Monster,
    winner: Monster,
    world: World,
}

export type FightReq = {
    date: Dayjs,
    loser: string,
    winner: string,
    world: string,
}

export type RequestParam = {
    name: string,
    value: string | number | null | string[],
    type: "text" | "number" | "autocomplete" | "date" | "checkbox"
    possibleValues?: string[]
}

export type PrintField = {
    name: string,
    value: string | number | null | string[],
}