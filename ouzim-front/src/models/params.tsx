import {RequestParam} from "./types";

export const MonsterParams: RequestParam[] = [
    {name: "name", value: null, type: "text"},
    {name: "id", value: null, type: "number"},
    {name: "description", value: null, type: "text"},
    {name: "type", value: null, type: "autocomplete", possibleValues: []},
    {name: "world", value: null, type: "autocomplete", possibleValues: []}
]
export const WorldParams: RequestParam[] = [
    {name: "name", value: null, type: "text"},
    {name: "id", value: null, type: "number"},
]
export const TypeParams: RequestParam[] = [
    {name: "name", value: null, type: "text"},
    {name: "id", value: null, type: "number"},
]
export const FightParams: RequestParam[] = [
    {name: "id", value: null, type: "number"},
    {name: "world", value: null, type: "autocomplete", possibleValues: []},
    {name: "winner", value: null, type: "text"},
    {name: "loser", value: null, type: "text"},
]