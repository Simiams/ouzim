import {FightReq, MonsterReq, TypeReq, WorldReq} from "./types";
import dayjs from "dayjs";

export const defaultMonster: MonsterReq = {
    name: "",
    description: "",
    url: "",
    world: "",
    types: []
}

export const defaultType: TypeReq = {
    name: ""
}

export const defaultWorld: WorldReq = {
    name: ""
}
export const defaultFight: FightReq = {
    date: dayjs(),
    loser: "",
    winner: "",
    world: "",
}