import React, {useEffect, useState} from 'react';
import {MonsterReq, RequestParam, Type, TypeReq} from "../models/types";
import {getTypes, postType} from "../api/type";
import {Button, IconButton, TextField} from "@mui/material";
import {defaultType} from "../models/default";
import FilterComponent from "../components/FilterComponent";
import {MonsterParams, TypeParams} from "../models/params";
import AddIcon from "@mui/icons-material/Add";
import SearchIcon from "@mui/icons-material/Search";
import {MonsterForm, TypeForm} from "../models/form";

function TypePage() {
    const [types, setTypes] = useState<Type[]>([])
    const [newType, setNewType] = useState<TypeReq>(defaultType)

    const [requestParams, setRequestParams] = useState<RequestParam[]>(TypeParams)
    const [requestForms, setRequestForms] = useState<RequestParam[]>(TypeForm)

    const [showCreateForm, setShowCreateForm] = useState<boolean>(false)
    const [showParamForm, setShowParamForm] = useState<boolean>(false)


    useEffect(() => {
        getTypes().then(res => setTypes(res.data))
    }, []);

    const createType = () => {
        postType(createTypeReq(requestForms)).then(res => setTypes([...types, res.data]))
    }

    const getTypesByParams = () => {
        getTypes(requestParams).then(res => setTypes(res.data))
    }

    const createTypeReq = (requestForm: RequestParam[]): TypeReq => {
        return {
            name: requestForm.filter(r => r.name === "name")[0].value as string,
        }
    }

    return (
        <div className={"page"}>


            {
                showCreateForm
                    ? <FilterComponent requestParams={requestForms} setRequestParams={setRequestForms}
                                       onSearch={createType} closeComponent={setShowCreateForm} componentType={"Création de type"}/>
                    : <IconButton
                        color="secondary"
                        aria-label="delete"
                        onClick={() => setShowCreateForm(true)}
                    >
                        <AddIcon/>
                    </IconButton>
            }

            {
                showParamForm ? <FilterComponent requestParams={requestParams} setRequestParams={setRequestParams}
                                                 onSearch={getTypesByParams} closeComponent={setShowParamForm} componentType={"Recherche de type"}/>
                    : <IconButton
                        color="secondary"
                        aria-label="delete"
                        onClick={() => setShowParamForm(true)}
                    >
                        <SearchIcon/>
                    </IconButton>
            }

            <div className="header">
                <div className="">Type</div>
            </div>
            {types.map((type) => <div className={"line"} key={type.name}>
                <div className="monster-name">{type.name}</div>
            </div>)}
        </div>
    )
}

export default TypePage