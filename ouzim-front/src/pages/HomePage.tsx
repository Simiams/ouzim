import React, {useEffect, useState} from 'react';
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import Box from "@mui/material/Box";
import MonsterPage from "./MonsterPage";
import TypePage from "./TypePage";
import WorldPage from "./WorldPage";
import FightPage from "./FightPage";
import {useAuth0} from "@auth0/auth0-react";
import {authLogin, getToken} from "../configurations/AuthConfig"
import "../css/global.css"
import {IconButton, TextField} from "@mui/material";
import TokenIcon from '@mui/icons-material/Token';

function HomePage() {
    const [tabValue, setTabValue] = useState<number>(0)

    const {user, isAuthenticated, getAccessTokenSilently, loginWithRedirect, logout} = useAuth0();

    const [userMetadata, setUserMetadata] = useState(null);

    const [hasToken, setHasToken] = useState(false)

    useEffect(() => {
        console.log(`${user?.sub}`)

        const getUserMetadata = async () => {
            const domain = "dev-nsxp1qfmvi1ykne1.us.auth0.com";
            try {
                const accessToken = await getAccessTokenSilently({
                    authorizationParams: {
                        audience: `https://${domain}/api/v2/`,
                        scope: "read:current_user",
                    },
                });

                authLogin(accessToken)
                setHasToken(true)


                const userDetailsByIdUrl = `https://${domain}/api/v2/users/${user?.sub}`;

                const metadataResponse = await fetch(userDetailsByIdUrl, {
                    headers: {
                        Authorization: `Bearer ${accessToken}`,
                    },
                });

                const {user_metadata} = await metadataResponse.json();

                setUserMetadata(user_metadata);
            } catch (e) {
                // @ts-ignore
                console.log(e.message);
            }
        };

        getUserMetadata();
    }, [getAccessTokenSilently, user?.sub]);

    return (
        <div className={"home-page"}>
            {
                (isAuthenticated && user !== undefined)
                    ? <React.Fragment>
                        <Box className={"tab-container"}>
                            <Tabs value={tabValue} onChange={(_, newValue) => setTabValue(newValue)} centered>
                                <Tab label="Monsters"/>
                                <Tab label="Types"/>
                                <Tab label="Worlds"/>
                                <Tab label="Fights"/>
                            </Tabs>
                        </Box>

                        <IconButton
                            color="secondary"
                            aria-label="delete"
                            onClick={() => alert(getToken())}
                        >
                            <TokenIcon/>
                        </IconButton>


                        {tabValue === 0 && <MonsterPage hasToken={hasToken}/>}
                        {tabValue === 1 && <TypePage/>}
                        {tabValue === 2 && <WorldPage/>}
                        {tabValue === 3 && <FightPage/>}
                    </React.Fragment>
                    : <div className={"login-container"}>
                        <button onClick={() => loginWithRedirect()}>Log In</button>
                    </div>
            }
        </div>
    )
}

export default HomePage