import React, {useEffect, useState} from 'react';
import {Fight, FightReq, Monster, RequestParam, World, WorldReq} from "../models/types";
import {getFights, postFight} from "../api/fight";
import {defaultFight} from "../models/default";
import {Autocomplete, Button, IconButton, TextField} from "@mui/material";
import {getMonsters} from "../api/monster";
import {getWorlds} from "../api/world";
import {DatePicker} from '@mui/x-date-pickers/DatePicker';
import dayjs, {Dayjs} from 'dayjs';
import {LocalizationProvider} from '@mui/x-date-pickers/LocalizationProvider';
import {AdapterDayjs} from '@mui/x-date-pickers/AdapterDayjs';
import FilterComponent from "../components/FilterComponent";
import {FightParams} from "../models/params";
import AddIcon from "@mui/icons-material/Add";
import SearchIcon from "@mui/icons-material/Search";
import {FightForm, TypeForm} from "../models/form";

function FightPage() {
    const [fights, setFights] = useState<Fight[]>([])
    const [monsters, setMonsters] = useState<string[]>([])
    const [worlds, setWorlds] = useState<string[]>([])

    const [newFight, setNewFight] = useState<FightReq>(defaultFight)

    const [requestParams, setRequestParams] = useState<RequestParam[]>(FightParams)
    const [requestForms, setRequestForms] = useState<RequestParam[]>(FightForm)

    const [showCreateForm, setShowCreateForm] = useState<boolean>(false)
    const [showParamForm, setShowParamForm] = useState<boolean>(false)


    useEffect(() => {
        getFights().then(res => setFights(res.data))
        getMonsters().then(res => setMonsters(res.data.map((m: Monster) => m.name)))
        getWorlds().then(res => setWorlds(res.data.map((w: World) => w.name)))
    }, []);

    useEffect(() => {
        const indexWorld = requestParams.findIndex(param => param.name === 'world')
        const updatedQueryParams = [...requestParams];
        updatedQueryParams[indexWorld] = {
            ...updatedQueryParams[indexWorld],
            possibleValues: worlds
        };
        setRequestParams(updatedQueryParams);
    }, [worlds]);


    useEffect(() => {
        const indexWorld = requestForms.findIndex(param => param.name === 'world')
        const updatedQueryParams = [...requestForms];
        updatedQueryParams[indexWorld] = {
            ...updatedQueryParams[indexWorld],
            possibleValues: worlds
        };
        setRequestForms(updatedQueryParams);
    }, [worlds]);

    const createFight = () => {
        postFight(createFightReq(requestForms)).then(res => setFights([...fights, res.data]))
    }

    const getFightsByParams = () => {
        getFights(requestParams).then(res => setFights(res.data))
    }

    const createFightReq = (requestForm: RequestParam[]): FightReq => {
        return {
            date: dayjs(),
            loser: requestForm.filter(r => r.name === "loser")[0].value as string,
            winner: requestForm.filter(r => r.name === "winner")[0].value as string,
            world: requestForm.filter(r => r.name === "world")[0].value as string,
        }
    }

    return (
        <div className={"page"}>

            {
                showCreateForm
                    ? <FilterComponent requestParams={requestForms} setRequestParams={setRequestForms}
                                       onSearch={createFight} closeComponent={setShowCreateForm} componentType={"Création combat"}/>
                    : <IconButton
                        color="secondary"
                        aria-label="delete"
                        onClick={() => setShowCreateForm(true)}
                    >
                        <AddIcon/>
                    </IconButton>
            }

            {
                showParamForm ? <FilterComponent requestParams={requestParams} setRequestParams={setRequestParams}
                                                 onSearch={getFightsByParams} closeComponent={setShowParamForm} componentType={"Recherche de combat"}/>
                    : <IconButton
                        color="secondary"
                        aria-label="delete"
                        onClick={() => setShowParamForm(true)}
                    >
                        <SearchIcon/>
                    </IconButton>
            }

            <div className="header">
                <div className="">Winner</div>
                <div className="">Loser</div>
                <div className="">World</div>
                <div className="">Date</div>
            </div>
            {fights.map((fight) => <div className={"line"} key={fight.id}>
                <div className="monster-name">{fight.winner.name}</div>
                <div className="monster-name">{fight.loser.name}</div>
                <div className="monster-name">{fight.world.name}</div>
                <div className="monster-name">
                    {dayjs(fight.date).format('DD/MM/YYYY')}
                </div>
            </div>)}
        </div>
    )
}

export default FightPage