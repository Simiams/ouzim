import React, {useEffect, useState} from 'react';
import {Monster, MonsterReq, RequestParam, Type, World} from "../models/types";
import {deleteMonster, getMonsters, patchMonster, postMonster, putMonster} from "../api/monster";
import {IconButton} from "@mui/material";
import {getWorlds} from "../api/world";
import {getTypes} from "../api/type";
import DeleteIcon from '@mui/icons-material/Delete';
import FilterComponent from "../components/FilterComponent";
import {MonsterParams} from "../models/params";
import {MonsterForm} from "../models/form";
import {getAttributeValue} from "../utils/utils";
import ModalComponent from "../components/ModalComponent";
import SearchIcon from '@mui/icons-material/Search';
import AddIcon from '@mui/icons-material/Add';

type MonsterPageProps = {
    hasToken: boolean
}

function MonsterPage({hasToken}: MonsterPageProps) {
    const [monsters, setMonsters] = useState<Monster[]>([])
    const [worlds, setWorlds] = useState<string[]>([])
    const [types, setTypes] = useState<string[]>([])

    const [openModal, setOpenModal] = useState<boolean>(false)

    const [requestParams, setRequestParams] = useState<RequestParam[]>(MonsterParams)
    const [requestForms, setRequestForms] = useState<RequestParam[]>(MonsterForm)
    const [entityModal, setEntityModal] = useState<RequestParam[]>(MonsterForm)
    const [entityModalId, setEntityModalId] = useState<number>(0)

    const [showCreateForm, setShowCreateForm] = useState<boolean>(false)
    const [showParamForm, setShowParamForm] = useState<boolean>(false)

    useEffect(() => {
        getMonsters().then(res => setMonsters(res.data))
        getWorlds().then(res => {
            setWorlds(res.data.map((world: World) => world.name))
        })
        getTypes().then(res => {
            setTypes(res.data.map((type: Type) => type.name))
        })
    }, [hasToken]);


    useEffect(() => {
        const indexType = requestParams.findIndex(param => param.name === 'type')
        const indexWorld = requestParams.findIndex(param => param.name === 'world')
        const updatedQueryParams = [...requestParams];
        updatedQueryParams[indexType] = {
            ...updatedQueryParams[indexType],
            possibleValues: types
        };
        updatedQueryParams[indexWorld] = {
            ...updatedQueryParams[indexWorld],
            possibleValues: worlds
        };
        setRequestParams(updatedQueryParams);
    }, [types, worlds]);

    useEffect(() => {
        const indexType = requestForms.findIndex(param => param.name === 'types')
        const indexWorld = requestForms.findIndex(param => param.name === 'world')
        const updatedQueryParams = [...requestForms];
        updatedQueryParams[indexType] = {
            ...updatedQueryParams[indexType],
            possibleValues: types
        };
        updatedQueryParams[indexWorld] = {
            ...updatedQueryParams[indexWorld],
            possibleValues: worlds
        };
        setRequestForms(updatedQueryParams);
    }, [types, worlds]);


    const createMonster = () => {
        postMonster(createMonsterReq(requestForms)).then(res => setMonsters([...monsters, res.data]))
    }
    const updateMonster = () => {
        putMonster(createMonsterReq(entityModal), entityModalId).then(res => setMonsters([...monsters.filter((m: Monster) => m.id !== res.data.id), res.data]))
    }
    const fixMonster = () => {
        patchMonster(createMonsterReq(entityModal), entityModalId).then(res => setMonsters([...monsters.filter((m: Monster) => m.id !== res.data.id), res.data]))
    }
    const removeMonster = (monster: Monster) => {
        deleteMonster(monster.id).then((_) => setMonsters(monsters.filter((m: Monster) => m.id !== monster.id)))
    }
    const getMonstersByParams = () => {
        getMonsters(requestParams).then(res => setMonsters(res.data))
    }

    const createMonsterReq = (requestForm: RequestParam[]): MonsterReq => {
        return {
            name: requestForm.filter(r => r.name === "name")[0].value as string,
            description: requestForm.filter(r => r.name === "description")[0].value as string,
            url: requestForm.filter(r => r.name === "url")[0].value as string,
            world: requestForm.filter(r => r.name === "world")[0].value as string,
            types: [...requestForm.filter(r => r.name === "types")[0].value as string[]],
        }
    }
    const createMonsterModal = (monster: Monster): void => {
        setEntityModal(requestForms.map((requestForm: RequestParam) => {
            let newValue: any = getAttributeValue(monster, requestForm.name as keyof Monster)
            if ((getAttributeValue(monster, requestForm.name as keyof Monster) as World).name !== undefined)
                newValue = (getAttributeValue(monster, requestForm.name as keyof Monster) as World).name
            if (Array.isArray(getAttributeValue(monster, requestForm.name as keyof Monster) as Type[]))
                newValue = (getAttributeValue(monster, requestForm.name as keyof Monster) as Type[]).map(type => type.name)
            return {
                ...requestForm,
                value: newValue
            }
        }))
    }


    return (
        <div className="page">

            {
                showCreateForm
                    ? <FilterComponent requestParams={requestForms} setRequestParams={setRequestForms}
                                       onSearch={createMonster} closeComponent={setShowCreateForm}
                                       componentType={"Création de monstre"}/>
                    : <IconButton
                        color="secondary"
                        aria-label="delete"
                        onClick={() => setShowCreateForm(true)}
                    >
                        <AddIcon/>
                    </IconButton>
            }

            {
                showParamForm ? <FilterComponent requestParams={requestParams} setRequestParams={setRequestParams}
                                                 onSearch={getMonstersByParams} closeComponent={setShowParamForm}
                                                 componentType={"Recherche de monstre"}/>
                    : <IconButton
                        color="secondary"
                        aria-label="delete"
                        onClick={() => setShowParamForm(true)}
                    >
                        <SearchIcon/>
                    </IconButton>
            }


            <div className="header">
                <div className="">Name</div>
                <div className="">Url</div>
                <div className="">Description</div>
                <div className="">World</div>
                <div className="">Types</div>
            </div>
            {monsters.map((monster) => <div className={"line"} key={monster.id}>
                <div className="line-content"
                     onClick={(_) => {
                         createMonsterModal(monster)
                         setOpenModal(true)
                         setEntityModalId(monster.id)
                     }}>
                    <div className="monster-name">{monster.name}</div>
                    <div className="monster-url">{monster.url}</div>
                    <div className="monster-description">{monster.description}</div>
                    <div className="monster-world">
                        <div className="world-name">{monster.world.name}</div>
                    </div>
                    <div className="monster-types">
                        {monster.types.map((type) => <div key={type.name}>
                            <div className="type-name">{type.name}</div>
                        </div>)}
                    </div>
                </div>
                <IconButton aria-label="delete" onClick={(_) => {
                    removeMonster(monster)
                }}>
                    <DeleteIcon/>
                </IconButton>
            </div>)}

            <ModalComponent openModal={openModal} setOpenModal={setOpenModal} entityModal={entityModal}
                            setEntityModal={setEntityModal} defaultEntity={requestForms} patch={fixMonster}
                            put={updateMonster}/>
        </div>
    )
}

export default MonsterPage