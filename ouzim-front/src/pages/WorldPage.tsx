import React, {useEffect, useState} from 'react';
import {Monster, RequestParam, TypeReq, World, WorldReq} from "../models/types";
import {deleteWorld, getWorlds, postWorld} from "../api/world";
import {Button, IconButton, TextField} from "@mui/material";
import {defaultWorld} from "../models/default";
import FilterComponent from "../components/FilterComponent";
import {TypeParams, WorldParams} from "../models/params";
import AddIcon from "@mui/icons-material/Add";
import SearchIcon from "@mui/icons-material/Search";
import {TypeForm, WorldForm} from "../models/form";
import DeleteIcon from "@mui/icons-material/Delete";
import {deleteMonster} from "../api/monster";

function WorldPage () {
    const [worlds, setWorlds] = useState<World[]>([])
    const [newWorld, setNewWorld] = useState<WorldReq>(defaultWorld)

    const [requestParams, setRequestParams] = useState<RequestParam[]>(WorldParams)
    const [requestForms, setRequestForms] = useState<RequestParam[]>(WorldForm)

    const [showCreateForm, setShowCreateForm] = useState<boolean>(false)
    const [showParamForm, setShowParamForm] = useState<boolean>(false)

    useEffect(() => {
        getWorlds().then(res => setWorlds(res.data))
    }, []);

    const createWorld = () => {
        postWorld(createWorldReq(requestForms)).then(res => setWorlds([...worlds, res.data]))
    }

    const getWorldsByParams = () => {
        getWorlds(requestParams).then(res => setWorlds(res.data))
    }

    const createWorldReq = (requestForm: RequestParam[]): WorldReq => {
        return {
            name: requestForm.filter(r => r.name === "name")[0].value as string,
        }
    }

    return (
        <div className={"page"}>

            {
                showCreateForm
                    ? <FilterComponent requestParams={requestForms} setRequestParams={setRequestForms}
                                       onSearch={createWorld} closeComponent={setShowCreateForm} componentType={"Création de monde"}/>
                    : <IconButton
                        color="secondary"
                        aria-label="delete"
                        onClick={() => setShowCreateForm(true)}
                    >
                        <AddIcon/>
                    </IconButton>
            }

            {
                showParamForm ? <FilterComponent requestParams={requestParams} setRequestParams={setRequestParams}
                                                 onSearch={getWorldsByParams} closeComponent={setShowParamForm} componentType={"Recherche de monde"}/>
                    : <IconButton
                        color="secondary"
                        aria-label="delete"
                        onClick={() => setShowParamForm(true)}
                    >
                        <SearchIcon/>
                    </IconButton>
            }

            <div className="header">
                <div className="">World</div>
            </div>
            {worlds.map((world) => <div className={"line"} key={world.name}>
                <div className="monster-name">{world.name}</div>
            </div>)}
        </div>
    )
}

export default WorldPage