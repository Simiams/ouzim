import {RequestParam} from "../models/types";

export const buildParams = (requestParams: RequestParam[] | undefined) => {
    return requestParams !== undefined
        ? "?" + requestParams
            .filter(param => param.value !== null && param.value !== "")
            .map(param => `${encodeURIComponent(param.name)}=${encodeURIComponent(Array.isArray(param.value) ? param.value[0] : param.value!)}`)
            .join("&")
        : ""
}
export const getAttributeValue = <T, K extends keyof T>(obj: T, key: K): T[K] => {
    return obj[key];
}
