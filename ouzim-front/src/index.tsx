import React from 'react';
import ReactDOM from 'react-dom/client';
import reportWebVitals from './reportWebVitals';

import {createBrowserRouter, RouterProvider} from "react-router-dom";
import HomePage from "./pages/HomePage";
import "./css/global.css"
import {Auth0Provider} from "@auth0/auth0-react";

const root = ReactDOM.createRoot(
    document.getElementById('root') as HTMLElement
);

const router = createBrowserRouter([
    {
        path: "/",
        element: <HomePage/>
    }
]);

root.render(
    <Auth0Provider
        domain="dev-nsxp1qfmvi1ykne1.us.auth0.com"
        clientId="Aq8df6PvTnKYkE6SqF5qlCmARvYHb50L"
        authorizationParams={{
            redirect_uri: window.location.origin,
            audience: "https://dev-nsxp1qfmvi1ykne1.us.auth0.com/api/v2/",
            scope: "read:current_user update:current_user_metadata"
        }}
        useRefreshTokens={true}
        cacheLocation={"localstorage"}
    >
        <React.StrictMode>
            <RouterProvider router={router}/>
        </React.StrictMode>
    </Auth0Provider>
);

reportWebVitals();
