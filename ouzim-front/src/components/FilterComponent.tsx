import {RequestParam} from "../models/types";
import {Autocomplete, Checkbox, IconButton, TextField} from "@mui/material";
import React from "react";
import {Unstable_NumberInput as BaseNumberInput,} from '@mui/base/Unstable_NumberInput';

import CheckBoxOutlineBlankIcon from '@mui/icons-material/CheckBoxOutlineBlank';
import CheckBoxIcon from '@mui/icons-material/CheckBox';
import DoneIcon from '@mui/icons-material/Done';
import CloseIcon from '@mui/icons-material/Close';

import "../css/filter-component.css"

type FilterComponentProps = {
    requestParams: RequestParam[],
    setRequestParams: (requestParams: RequestParam[]) => void,
    onSearch: () => void,
    closeComponent?: (value: boolean) => void,
    componentType?: string
}
const FilterComponent = ({
                             requestParams,
                             setRequestParams,
                             onSearch,
                             closeComponent,
                             componentType
                         }: FilterComponentProps) => {

    const handleChange = (index: number, newValue: string | number | null | string[]) => {
        const updatedQueryParams = [...requestParams];
        updatedQueryParams[index] = {...updatedQueryParams[index], value: newValue};
        setRequestParams(updatedQueryParams);
    };

    return (
        <div className={"filter-component"}>

            {
                componentType !== undefined && <div className={"filter-component-title"}>
                    {componentType}
                </div>
            }


            <div className={"filter-component-body"}>

                {
                    closeComponent !== undefined &&
                    <IconButton aria-label="delete" onClick={() => closeComponent(false)}>
                        <CloseIcon/>
                    </IconButton>
                }

                {requestParams.map((param, index) => (
                        <div className={"input-base"} key={param.name}>
                            {param.type === "text" &&
                                <TextField
                                    label={param.name}
                                    variant="outlined"
                                    value={param.value}
                                    onChange={e => handleChange(index, e.target.value)}
                                />
                            }
                            {param.type === "number" &&
                                <BaseNumberInput
                                    placeholder={param.name}
                                    className={"number-imput"}
                                    min={0}
                                    max={100}
                                    slotProps={{
                                        incrementButton: {
                                            children: '+',
                                        },
                                        decrementButton: {
                                            children: '-',
                                        },
                                    }}
                                    onChange={(_, value) => handleChange(index, value)}
                                />
                            }
                            {param.type === "autocomplete" &&
                                <Autocomplete
                                    value={param.value as string}
                                    onChange={(_, newValue) => handleChange(index, newValue)}
                                    disablePortal
                                    options={param.possibleValues!}
                                    sx={{width: 300}}
                                    renderInput={(params) => <TextField {...params} label={param.name}/>}
                                />
                            }
                            {param.type === "checkbox" &&
                                <Autocomplete
                                    value={Array.isArray(param.value) ? param.value : [(param.value as string)]}
                                    onChange={(_, newValues) => handleChange(index, newValues)}
                                    multiple
                                    id="checkboxes-tags-demo"
                                    options={param.possibleValues!}
                                    disableCloseOnSelect
                                    getOptionLabel={(option) => option}
                                    renderOption={(props, option, {selected}) => (
                                        <li {...props}>
                                            <Checkbox
                                                icon={<CheckBoxOutlineBlankIcon fontSize="small"/>}
                                                checkedIcon={<CheckBoxIcon fontSize="small"/>}
                                                style={{marginRight: 8}}
                                                checked={selected}
                                            />
                                            {option}
                                        </li>
                                    )}
                                    style={{width: 500}}
                                    renderInput={(params) => (
                                        <TextField {...params} label={"Types"}/>
                                    )}
                                />
                            }
                        </div>
                    )
                )}

                <IconButton aria-label="delete" onClick={onSearch}>
                    <DoneIcon/>
                </IconButton>
            </div>
        </div>
    )
}

export default FilterComponent