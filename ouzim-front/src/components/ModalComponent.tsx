import {IconButton} from "@mui/material";
import Modal from "@mui/material/Modal";
import Box from "@mui/material/Box";
import FilterComponent from "./FilterComponent";
import DeleteIcon from "@mui/icons-material/Delete";
import React, {useState} from "react";
import {RequestParam} from "../models/types";
import "../css/modal-component.css"

type ModalComponentProps = {
    openModal: boolean,
    setOpenModal: (open: boolean) => void,
    entityModal: RequestParam[],
    setEntityModal: (entityModal: RequestParam[]) => void,
    defaultEntity: RequestParam[],
    put: () => void,
    patch: () => void,
}

const ModalComponent = ({
                            openModal,
                            setOpenModal,
                            entityModal,
                            setEntityModal,
                            defaultEntity,
                            put,
                            patch
                        }: ModalComponentProps) => {

    const [isDeleted, setIsDeleted] = useState<boolean>(false)

    return (
        <Modal
            className={"modal-component"}
            open={openModal}
            onClose={(_) => setOpenModal(false)}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box className={"modal-component-body"}>
                <FilterComponent requestParams={entityModal} setRequestParams={setEntityModal}
                                 onSearch={isDeleted ? put : patch}/>
                <IconButton aria-label="delete" onClick={(_) => {
                    setIsDeleted(true)
                    setEntityModal(defaultEntity)
                }}>
                    <DeleteIcon/>
                </IconButton>
            </Box>
        </Modal>)
}

export default ModalComponent