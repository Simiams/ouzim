export const authLogin = (token: string) => {
    localStorage.setItem('token', token)
};

export const authLogout = () => {
    localStorage.removeItem('token')
};

export const isAuthenticated = () => {
    return localStorage.getItem('token') !== null && localStorage.getItem('token') !== "";
};

export const getToken = () => {
    return localStorage.getItem('token')
}

