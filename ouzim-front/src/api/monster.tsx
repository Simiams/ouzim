import axiosInterceptor from './axiosInterceptor';
import {monsterUrl} from "./baseUrl";
import {MonsterReq, RequestParam} from "../models/types";
import {buildParams} from "../utils/utils";

export const getMonsters = (requestParams?: RequestParam[]) => axiosInterceptor.get(`${monsterUrl}${buildParams(requestParams)}`);
export const postMonster = (monster: MonsterReq) => axiosInterceptor.post(`${monsterUrl}`, monster)
export const putMonster = (monster: MonsterReq, id:number) => axiosInterceptor.put(`${monsterUrl}/${id}`, monster)
export const patchMonster = (monster: MonsterReq, id:number) => axiosInterceptor.patch(`${monsterUrl}/${id}`, monster)
export const deleteMonster = (id:number) => axiosInterceptor.delete(`${monsterUrl}/${id}`)