const backDomain = process.env.REACT_APP_BACK_DOMAIN;
const backport = process.env.REACT_APP_BACK_PORT;

export const monsterUrl = `${backDomain}:${backport}/monsters`
export const typeUrl = `${backDomain}:${backport}/types`
export const worldUrl = `${backDomain}:${backport}/worlds`
export const fightUrl = `${backDomain}:${backport}/fights`
