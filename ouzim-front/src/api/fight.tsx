import axiosInterceptor from './axiosInterceptor';
import {fightUrl, monsterUrl} from "./baseUrl";
import {FightReq, RequestParam} from "../models/types";
import {buildParams} from "../utils/utils";

export const getFights = (requestParams?: RequestParam[]) => axiosInterceptor.get(`${fightUrl}${buildParams(requestParams)}`);
export const postFight = (fight: FightReq) => axiosInterceptor.post(`${fightUrl}`, fight)