import axiosInterceptor from './axiosInterceptor';
import {worldUrl} from "./baseUrl";
import {RequestParam, WorldReq} from "../models/types";
import {buildParams} from "../utils/utils";

export const getWorlds = (requestParams?: RequestParam[]) => axiosInterceptor.get(`${worldUrl}${buildParams(requestParams)}`)
export const postWorld = (world: WorldReq) => axiosInterceptor.post(`${worldUrl}`, world)
export const deleteWorld = (id: number) => axiosInterceptor.delete(`${worldUrl}${id}`)