import axios from 'axios';
import {getToken} from "../configurations/AuthConfig";

const axiosInterceptor = axios.create();

axiosInterceptor.interceptors.request.use(
    (config) => {
        const token = getToken();
        if (token) {
            config.headers.Authorization = `Bearer ${token}`;
        }
        return config;
    },
    (error) => {
        return Promise.reject(error);
    }
);
// axiosInterceptor.interceptors.response.use(
//     (response) => {
//         return response;
//     },
//     (error) => {
//         if (error.response && error.response.status === 403) {
//             window.location.href = '/';
//         }
//         return Promise.reject(error);
//     }
// );

export default axiosInterceptor;
