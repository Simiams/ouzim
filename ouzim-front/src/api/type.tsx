import axiosInterceptor from './axiosInterceptor';
import {typeUrl} from "./baseUrl";
import {RequestParam, TypeReq} from "../models/types";
import {buildParams} from "../utils/utils";

export const getTypes = (requestParams?: RequestParam[]) => axiosInterceptor.get(`${typeUrl}${buildParams(requestParams)}`)
export const postType = (type: TypeReq) => axiosInterceptor.post(`${typeUrl}`, type)