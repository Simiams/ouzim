CREATE TABLE users
(
    id       SERIAL PRIMARY KEY,
    username VARCHAR(255) NOT NULL,
    password TEXT         NOT NULL
);

CREATE TABLE refresh_tokens
(
    id       SERIAL PRIMARY KEY,
    token    VARCHAR(255) NOT NULL,
    username VARCHAR(50)  NOT NULL
);
