package fr.simsim.ouzimapi.repositories;

import fr.simsim.ouzimapi.models.Type;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface TypeRepository extends JpaRepository<Type, Long> {
    Type findByName(String name);

    @Query("SELECT t FROM Type t WHERE " +
            "(:id is null or t.id = :id) AND " +
            "(:name is null or t.name ILIKE :name)")
    List<Type> findTypesByParameters(Long id, String name);
}
