package fr.simsim.ouzimapi.repositories;

import fr.simsim.ouzimapi.models.World;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WorldRepository extends JpaRepository<World, Long> {
    World findByName(String name);


    @Query("SELECT w FROM World w WHERE " +
            "(:id is null or w.id = :id) AND " +
            "(:name is null or w.name ILIKE :name)")
    List<World> findWorldByParameters(Long id, String name);
}