package fr.simsim.ouzimapi.repositories;

import fr.simsim.ouzimapi.models.Fight;
import fr.simsim.ouzimapi.models.Monster;
import fr.simsim.ouzimapi.models.World;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface FightRepository extends JpaRepository<Fight, Long> {
    void deleteByWinnerOrLoser(Monster winner, Monster loser);

    @Query("SELECT f FROM Fight f WHERE " +
            "(:id is null or f.id = :id) AND " +
            "(:loser is null or f.loser = :loser) AND " +
            "(:winner is null or f.winner = :winner) AND " +
            "(:world is null or f.world = :world) AND " +
            "(:date is null or f.date = :date)")
    List<Fight> findFightsByParameters(
            @Param("id") Long id,
            @Param("loser") Monster loserEntity,
            @Param("winner") Monster winnerEntity,
            @Param("world") World worldEntity,
            @Param("date") Date date
    );
}