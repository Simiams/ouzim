package fr.simsim.ouzimapi.repositories;

import fr.simsim.ouzimapi.models.Monster;
import fr.simsim.ouzimapi.models.Type;
import fr.simsim.ouzimapi.models.World;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface MonsterRepository extends JpaRepository<Monster, Long> {
    Monster findByName(String loser);

    @Query("SELECT m FROM Monster m WHERE " +
            "(:id is null or m.id = :id) AND " +
            "(:description is null or m.description ILIKE :description%) AND " +
            "(:name is null or m.name ILIKE :name) AND " +
            "(:url is null or m.url = :url) AND " +
            "(:world is null or m.world = :world) AND " +
            "(:type is null or :type member of m.types)")
    List<Monster> findMonstersByParameters(
            @Param("id") Long id,
            @Param("description") String description,
            @Param("name") String name,
            @Param("url") String url,
            @Param("world") World world,
            @Param("type") Type type
    );
}
