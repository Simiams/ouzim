package fr.simsim.ouzimapi.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;


@NoArgsConstructor
@AllArgsConstructor
@Entity
@Data
@Builder
@Table(name = "fights")
public class Fight {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    @JoinColumn(name = "winner_id", nullable = false)
    private Monster winner;
    @ManyToOne
    @JoinColumn(name = "loser_id", nullable = false)
    private Monster loser;
    @ManyToOne
    @JoinColumn(name = "world_id", nullable = false)
    private World world;
    @Temporal(TemporalType.DATE)
    private Date date;
}
