package fr.simsim.ouzimapi.dtos.requests;

import fr.simsim.ouzimapi.models.Monster;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MonsterRequest {
    private String name;
    private String url;
    private String description;
    private String world;
    private List<String> types;

    public Monster toMonster() {
        return Monster.builder()
                .name(name)
                .url(url)
                .description(description)
                .build();
    }
}
