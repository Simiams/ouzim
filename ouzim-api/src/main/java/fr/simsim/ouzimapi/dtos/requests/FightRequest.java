package fr.simsim.ouzimapi.dtos.requests;

import fr.simsim.ouzimapi.models.Fight;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@NoArgsConstructor
@Data
public class FightRequest {
    private String winner;
    private String loser;
    private String world;
    private Date date;

    public Fight toFight() {
        return Fight.builder()
                .date(this.date)
                .build();
    }
}
