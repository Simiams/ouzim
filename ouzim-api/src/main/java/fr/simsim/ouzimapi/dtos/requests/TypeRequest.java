package fr.simsim.ouzimapi.dtos.requests;

import fr.simsim.ouzimapi.models.Monster;
import fr.simsim.ouzimapi.models.Type;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TypeRequest {
    private String name;

    public Type toType() {
        return Type.builder()
                .name(name)
                .build();
    }
}
