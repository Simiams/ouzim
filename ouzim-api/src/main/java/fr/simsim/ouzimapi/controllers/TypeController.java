package fr.simsim.ouzimapi.controllers;

import fr.simsim.ouzimapi.dtos.requests.TypeRequest;
import fr.simsim.ouzimapi.models.Type;
import fr.simsim.ouzimapi.repositories.TypeRepository;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.apache.coyote.BadRequestException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/types")
@AllArgsConstructor
public class TypeController {
    private TypeRepository typeRepository;

    @GetMapping("")
    public List<Type> getTypes(
            @RequestParam(required = false) Long id,
            @RequestParam(required = false) String name
    ){
        return typeRepository.findTypesByParameters(id, name);
    }


    @SneakyThrows
    @GetMapping("/{id}")
    public Type getTypeById(@PathVariable long id){
        return typeRepository.findById(id)
                .orElseThrow(() -> new BadRequestException("Grade not found"));
    }

    @PostMapping("")
    public Type createType(@RequestBody TypeRequest typeRequest){
        return typeRepository.save(typeRequest.toType());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteType(@PathVariable long id){
        typeRepository.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @PutMapping("/{id}")
    public ResponseEntity<Type> updateType(@PathVariable long id, @RequestBody TypeRequest newType) {
        return typeRepository.findById(id)
                .map(type -> {
                    type.setName(newType.getName());
                    Type updatedType = typeRepository.save(type);
                    return ResponseEntity.ok(updatedType);
                })
                .orElseGet(() -> ResponseEntity.notFound().build());
    }
}
