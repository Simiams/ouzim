package fr.simsim.ouzimapi.controllers;

import fr.simsim.ouzimapi.dtos.requests.FightRequest;
import fr.simsim.ouzimapi.dtos.requests.MonsterRequest;
import fr.simsim.ouzimapi.exceptions.BadRequestException;
import fr.simsim.ouzimapi.models.Fight;
import fr.simsim.ouzimapi.models.Monster;
import fr.simsim.ouzimapi.models.World;
import fr.simsim.ouzimapi.repositories.FightRepository;
import fr.simsim.ouzimapi.repositories.MonsterRepository;
import fr.simsim.ouzimapi.repositories.TypeRepository;
import fr.simsim.ouzimapi.repositories.WorldRepository;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.swing.text.html.Option;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@RestController
@RequestMapping("/fights")
@AllArgsConstructor
public class FightController {
    private FightRepository fightRepository;
    private MonsterRepository monsterRepository;
    private WorldRepository worldRepository;

    @GetMapping("")
    public List<Fight> getFights(
            @RequestParam(required = false) Long id,
            @RequestParam(required = false) String loser,
            @RequestParam(required = false) String winner,
            @RequestParam(required = false) String world,
            @RequestParam(required = false) Date date
            ){
        Monster loserEntity = monsterRepository.findByName(loser);
        Monster winnerEntity = monsterRepository.findByName(winner);
        World worldEntity = worldRepository.findByName(world);
        return fightRepository.findFightsByParameters(id, loserEntity, winnerEntity, worldEntity, date);
    }

    @GetMapping("/{id}")
    public Fight getFightById(@PathVariable long id){
        return fightRepository.findById(id).orElse(null);
    }

    @PostMapping("")
    public Fight createFight(@RequestBody FightRequest fight){
        Fight newFight = fight.toFight();
        World world = worldRepository.findByName(fight.getWorld());
        Monster loser = monsterRepository.findByName(fight.getLoser());
        Monster winner = monsterRepository.findByName(fight.getWinner());
        if (world == null)
            throw new BadRequestException("World not found");
        if (loser == null || winner == null)
            throw new BadRequestException("Monster not found");
        newFight.setWorld(world);
        newFight.setLoser(loser);
        newFight.setWinner(winner);
        return fightRepository.save(newFight);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteFight(@PathVariable long id){
        fightRepository.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @PutMapping("/{id}")
    public ResponseEntity<Fight> updateFight(@PathVariable long id, @RequestBody Fight newFight) {
        return fightRepository.findById(id)
                .map(fight -> {
                    fight.setWinner(newFight.getWinner());
                    fight.setLoser(newFight.getLoser());
                    fight.setWorld(newFight.getWorld());
                    fight.setDate(newFight.getDate());
                    Fight updatedFight = fightRepository.save(fight);
                    return ResponseEntity.ok(updatedFight);
                })
                .orElseGet(() -> ResponseEntity.notFound().build());
    }
}
