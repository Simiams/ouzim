package fr.simsim.ouzimapi.controllers;

import fr.simsim.ouzimapi.models.World;
import fr.simsim.ouzimapi.repositories.WorldRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/worlds")
@AllArgsConstructor
public class WorldController {
    private WorldRepository worldRepository;

    @GetMapping
    public List<World> getAllWorlds(
            @RequestParam(required = false) Long id,
            @RequestParam(required = false) String name
    ) {
        return worldRepository.findWorldByParameters(id, name);
    }

    @GetMapping("/{id}")
    public ResponseEntity<World> getWorldById(@PathVariable Long id) {
        Optional<World> world = worldRepository.findById(id);
        return world.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping
    public World createWorld(@RequestBody World world) {
        return worldRepository.save(world);
    }

    @PutMapping("/{id}")
    public ResponseEntity<World> updateWorld(@PathVariable Long id, @RequestBody World worldDetails) {
        Optional<World> worldOptional = worldRepository.findById(id);
        if (worldOptional.isPresent()) {
            World world = worldOptional.get();
            world.setName(worldDetails.getName());
            return ResponseEntity.ok(worldRepository.save(world));
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteWorld(@PathVariable Long id) {
        Optional<World> worldOptional = worldRepository.findById(id);
        if (worldOptional.isPresent()) {
            worldRepository.delete(worldOptional.get());
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
