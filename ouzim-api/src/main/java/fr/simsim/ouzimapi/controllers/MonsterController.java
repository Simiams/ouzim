package fr.simsim.ouzimapi.controllers;

import fr.simsim.ouzimapi.dtos.requests.MonsterRequest;
import fr.simsim.ouzimapi.exceptions.BadRequestException;
import fr.simsim.ouzimapi.models.Monster;
import fr.simsim.ouzimapi.models.Type;
import fr.simsim.ouzimapi.models.World;
import fr.simsim.ouzimapi.repositories.FightRepository;
import fr.simsim.ouzimapi.repositories.MonsterRepository;
import fr.simsim.ouzimapi.repositories.TypeRepository;
import fr.simsim.ouzimapi.repositories.WorldRepository;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/monsters")
@AllArgsConstructor
public class MonsterController {
    private MonsterRepository monsterRepository;
    private TypeRepository typeRepository;
    private WorldRepository worldRepository;
    private FightRepository fightRepository;

    @GetMapping("")
    public List<Monster> getMonsters(
            @RequestParam(required = false) Long id,
            @RequestParam(required = false) String description,
            @RequestParam(required = false) String name,
            @RequestParam(required = false) String url,
            @RequestParam(required = false) String world,
            @RequestParam(required = false) String type
    ) {
        Type typeEntity = typeRepository.findByName(type);
        World worldEntity = worldRepository.findByName(world);
        return monsterRepository.findMonstersByParameters(id, description, name, url, worldEntity, typeEntity);
    }

    @GetMapping("/{id}")
    public Monster getMonsterById(@PathVariable long id) {
        return monsterRepository.findById(id).orElse(null);
    }

    @PostMapping("")
    public Monster createMonster(@RequestBody MonsterRequest monster) {
        Monster monsterEntity = monster.toMonster();
        World world = worldRepository.findByName(monster.getWorld());
        Set<Type> types = monster.getTypes().stream().map(type ->
                typeRepository.findByName(type)).collect(Collectors.toSet());
        if (world == null)
            throw new BadRequestException("World not found");
        if (types.stream().anyMatch(Objects::isNull))
            throw new BadRequestException("Types not found");
        monsterEntity.setWorld(world);
        monsterEntity.setTypes(types);
        return monsterRepository.save(monsterEntity);
    }

    @DeleteMapping("/{id}")
    @Transactional
    public ResponseEntity<Void> deleteMonster(@PathVariable long id) {
        Monster monster = monsterRepository.findById(id)
                .orElseThrow(() -> new BadRequestException("Monster not found"));
        fightRepository.deleteByWinnerOrLoser(monster, monster);
        monsterRepository.deleteById(id);
        return ResponseEntity.noContent().build();
    }


    @PutMapping("/{id}")
    public ResponseEntity<Monster> updateMonster(@PathVariable long id, @RequestBody MonsterRequest monsterDetails) {
        Monster monster = monsterRepository.findById(id)
                .orElseThrow(() -> new BadRequestException("Monster not found"));
        World world = worldRepository.findByName(monsterDetails.getWorld());
        if (world == null)
            throw new BadRequestException("World not found");
        Set<Type> types = monsterDetails.getTypes().stream().map(typeName ->
                        typeRepository.findByName(typeName))
                .filter(Objects::nonNull)
                .collect(Collectors.toSet());
        monster.setName(monsterDetails.getName());
        monster.setDescription(monsterDetails.getDescription());
        monster.setUrl(monsterDetails.getUrl());
        monster.setWorld(world);
        monster.setTypes(types);
        Monster updatedMonster = monsterRepository.save(monster);
        return ResponseEntity.ok(updatedMonster);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<Monster> patchMonster(@PathVariable long id, @RequestBody MonsterRequest monsterDetails) {
        Monster monster = monsterRepository.findById(id)
                .orElseThrow(() -> new BadRequestException("Monster not found"));
        World world = worldRepository.findByName(monsterDetails.getWorld());

        Set<Type> types = monsterDetails.getTypes() == null ? monster.getTypes() : monsterDetails.getTypes().stream().map(typeName ->
                        typeRepository.findByName(typeName))
                .collect(Collectors.toSet());

        monster.setName(monsterDetails.getName() == null ? monster.getName() : monsterDetails.getName());
        monster.setDescription(monsterDetails.getDescription() == null ? monster.getDescription() : monsterDetails.getDescription());
        monster.setUrl(monsterDetails.getUrl() == null ? monster.getUrl() : monsterDetails.getUrl());
        monster.setWorld(monsterDetails.getWorld() == null ? monster.getWorld() : world);
        monster.setTypes(types);
        Monster updatedMonster = monsterRepository.save(monster);
        return ResponseEntity.ok(updatedMonster);
    }
}
