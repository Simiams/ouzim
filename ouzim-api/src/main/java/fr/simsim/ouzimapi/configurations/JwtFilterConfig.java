package fr.simsim.ouzimapi.configurations;


import fr.simsim.ouzimapi.clients.AuthClient;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.time.Duration;
import java.time.Instant;

@Component
public class JwtFilterConfig extends OncePerRequestFilter {

    private final AuthClient authClient;
    private Instant lastRecordedTime;
    private String lastToken;

    public JwtFilterConfig(AuthClient authClient) {
        this.authClient = authClient;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        try {
            String token = extractToken(request);
            if (validateToken(token))
                SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken("wrong", null, null));
        } catch (Exception e) {
            logger.error("Erreur lors de la validation du token JWT", e);
        }
        filterChain.doFilter(request, response);
    }

    private String extractToken(HttpServletRequest request) {
        String bearerToken = request.getHeader("Authorization");
        if (StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer ")) {
            return bearerToken.substring(7);
        }
        return null;
    }

    private boolean validateToken(String token) {
        lastRecordedTime = Instant.now();
        if (lastRecordedTime == null || lastToken == null || !lastToken.equals(token)) {
            lastToken = token;
            return authClient.validateToken(token);
        }
        Duration duration = Duration.between(lastRecordedTime, Instant.now());
        lastToken = token;
        return duration.toMinutes() > 10 ? authClient.validateToken(token): true;
    }
}