package fr.simsim.ouzimapi.resolvers;

import fr.simsim.ouzimapi.dtos.requests.TypeRequest;
import fr.simsim.ouzimapi.models.Type;
import fr.simsim.ouzimapi.repositories.TypeRepository;
import graphql.kickstart.tools.GraphQLMutationResolver;
import graphql.kickstart.tools.GraphQLQueryResolver;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Component
public class TypeResolver implements GraphQLQueryResolver, GraphQLMutationResolver {

    private final TypeRepository typeRepository;

    public TypeResolver(TypeRepository typeRepository) {
        this.typeRepository = typeRepository;
    }

    public List<Type> types() {
        return typeRepository.findAll();
    }

    public Type getTypeById(@PathVariable long id) {
        return typeRepository.findById(id).orElse(null);
    }

    public Type createType(@RequestBody TypeRequest typeRequest) {
        return typeRepository.save(typeRequest.toType());
    }

    public Boolean deleteType(@PathVariable long id) {
        typeRepository.deleteById(id);
        return true;
    }

    public Type updateType(@PathVariable long id, @RequestBody TypeRequest newType) {
        return typeRepository.findById(id)
                .map(type -> {
                    type.setName(newType.getName());
                    return typeRepository.save(type);
                })
                .orElseGet(() -> new Type());
    }
}
