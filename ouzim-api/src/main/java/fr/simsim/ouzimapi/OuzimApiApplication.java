package fr.simsim.ouzimapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OuzimApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(OuzimApiApplication.class, args);
    }

}
