package fr.simsim.ouzimapi.exceptions;

public class InternException extends RuntimeException {

    public InternException(String message) {
        super(message);
    }
}
