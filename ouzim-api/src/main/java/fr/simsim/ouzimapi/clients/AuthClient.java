package fr.simsim.ouzimapi.clients;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Component
public class AuthClient {

    private final WebClient.Builder webClientBuilder;

    @Value("${service.auth.url}")
    private String authServiceUrl;
    private final String URI = "/validate";

    public AuthClient(WebClient.Builder webClientBuilder) {
        this.webClientBuilder = webClientBuilder;
    }

    public Boolean validateToken(String token) {
        return webClientBuilder.build()
                .get()
                .uri(authServiceUrl + "/userinfo")
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + token)
                .retrieve()
                .toBodilessEntity()
                .flatMap(responseEntity -> {
                    HttpStatusCode statusCode = responseEntity.getStatusCode();
                    return Mono.just(statusCode == HttpStatus.OK);
                })
                .onErrorReturn(false)
                .block();
    }
}
