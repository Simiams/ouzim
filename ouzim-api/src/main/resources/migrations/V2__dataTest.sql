INSERT INTO worlds (name)
VALUES ('World 1'),
       ('World 2'),
       ('World 3');

INSERT INTO monsters (name, description, url, world_id)
VALUES ('Dragon', 'A fierce and powerful creature', 'https://example.com/dragon.jpg', 1),
       ('Goblin', 'A small, mischievous creature', 'https://example.com/goblin.jpg', 2),
       ('Troll', 'A large, brutish creature', 'https://example.com/troll.jpg', 3);

INSERT INTO types (name)
VALUES ('Fire'),
       ('Water'),
       ('Earth');

INSERT INTO monster_type (monster_id, type_id)
VALUES (1, 1);
INSERT INTO monster_type (monster_id, type_id)
VALUES (2, 2);
INSERT INTO monster_type (monster_id, type_id)
VALUES (3, 3);

INSERT INTO fights (world_id, winner_id, loser_id, date)
VALUES (1, 1, 2, '2024-05-23');
INSERT INTO fights (world_id, winner_id, loser_id, date)
VALUES (2, 2, 3, '2024-05-24');


