CREATE TABLE test.worlds
(
    id   SERIAL PRIMARY KEY,
    name VARCHAR(255) NOT NULL
);

CREATE TABLE test.types
(
    id   SERIAL PRIMARY KEY,
    name VARCHAR(255) NOT NULL
);

CREATE TABLE test.monsters
(
    id          SERIAL PRIMARY KEY,
    name        VARCHAR(50),
    description VARCHAR(255),
    url         VARCHAR(255),
    world_id    INT NOT NULL,
    FOREIGN KEY (world_id) REFERENCES worlds (id)
);

CREATE TABLE test.monster_type
(
    monster_id INT NOT NULL,
    type_id    INT NOT NULL,
    PRIMARY KEY (monster_id, type_id),
    FOREIGN KEY (monster_id) REFERENCES monsters (id),
    FOREIGN KEY (type_id) REFERENCES types (id)
);



CREATE TABLE test.fights
(
    id        SERIAL PRIMARY KEY,
    world_id  INT NOT NULL,
    winner_id INT NOT NULL,
    loser_id  INT NOT NULL,
    date      DATE,
    FOREIGN KEY (world_id) REFERENCES worlds (id),
    FOREIGN KEY (winner_id) REFERENCES monsters (id),
    FOREIGN KEY (loser_id) REFERENCES monsters (id)
);
