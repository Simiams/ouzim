import requests
import re
from bs4 import BeautifulSoup
import time

from models.Monster import Monster

request_count = 0


def scrape_monster_description(uri, name):
    print(str(request_count) + " - SCRAPE_MONSTER_DESCRIPTION == GET " + uri)
    check_request_count()
    response = requests.get("https://fr.wikipedia.org/" + uri)
    response.encoding = response.apparent_encoding
    if response.status_code != 200:
        print(f"Failed to retrieve the page. Status code: {response.status_code}")
        return

    for child in BeautifulSoup(response.content, "html.parser").find("div",
                                                                     class_="mw-content-ltr mw-parser-output").find_all(
            "p"):
        if name.upper() in child.text.upper():
            return child.text.strip()


def scrape_monsters(url):
    print(str(request_count) + " - SCRAPE_MONSTERS == GET " + url)
    check_request_count()
    response = requests.get(url)
    response.encoding = response.apparent_encoding
    if response.status_code != 200:
        print(f"Failed to retrieve the page. Status code: {response.status_code}")
        return

    soup = BeautifulSoup(response.content, "html.parser").find("div", id="mw-pages")
    monsters = []

    for summary in soup.find_all(class_="mw-category-group"):
        if summary.find("h3").string != "*":
            for link in summary.find_all("a"):
                monsters.append(Monster(name=re.sub(r'\([^)]*\)', '', link.string), url=link.get("href"),
                                        description=scrape_monster_description(link.get("href"),
                                                                               re.sub(r'\([^)]*\)', '', link.string))))
    return monsters


def check_request_count():
    global request_count
    if request_count % 15 == 0 and request_count != 0:
        print("WAIT FOR 5 SECONDES")
        time.sleep(5)
    request_count += 1
