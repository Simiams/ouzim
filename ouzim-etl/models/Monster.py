class Monster:
    def __init__(self, name, url, description=None, image_url=None):
        self.name = name
        self.description = description
        self.image_url = image_url
        self.url = url

    def __repr__(self):
        return f"Monster(name={self.name}, url={self.url}, description={self.description}, image_url={self.image_url})"

    def to_dict(self):
        return {
            "name": self.name,
            "url": self.url,
            "description": self.description,
            "image_url": self.image_url
        }
