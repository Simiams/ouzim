class World:
    def __init__(self, name, url, description=None, image_url=None, monsters=None):
        self.name = name
        self.description = description
        self.image_url = image_url
        self.monsters = monsters
        self.url = url

    def __repr__(self):
        return f"World(name={self.name}, url={self.url}, description={self.description}, image_url={self.image_url}, monsters={self.monsters})"

    def to_dict(self):
        return {
            "name": self.name,
            "url": self.url,
            "description": self.description,
            "image_url": self.image_url,
            "monsters": [monster.to_dict() for monster in self.monsters]
        }
