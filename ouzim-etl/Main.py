from Etl import scrape_monsters
from models.World import World
import json

worlds_dict = {
    "grec": "https://fr.wikipedia.org/wiki/Cat%C3%A9gorie:Cr%C3%A9ature_fantastique_de_la_mythologie_grecque",
    "rome": "https://fr.wikipedia.org/wiki/Cat%C3%A9gorie:Cr%C3%A9ature_fantastique_de_la_mythologie_romaine",
    "nordic": "https://fr.wikipedia.org/wiki/Cat%C3%A9gorie:Mythologie_nordique",
    "yokai": "https://fr.wikipedia.org/wiki/Cat%C3%A9gorie:Y%C5%8Dkai",
    "celltic": "https://fr.wikipedia.org/wiki/Cat%C3%A9gorie:Cr%C3%A9ature_de_la_mythologie_celtique",
    "hindou": "https://fr.wikipedia.org/wiki/Cat%C3%A9gorie:Cr%C3%A9ature_fantastique_de_la_mythologie_hindoue",
    "chinois": "https://fr.wikipedia.org/wiki/Cat%C3%A9gorie:Cr%C3%A9ature_de_la_mythologie_chinoise",
    "afrique": "https://fr.wikipedia.org/wiki/Catégorie:Créature_fantastique_d'Afrique",
    "amerindienne": "https://fr.wikipedia.org/wiki/Cat%C3%A9gorie:Cr%C3%A9ature_de_la_mythologie_am%C3%A9rindienne",
    "polynesie": "https://fr.wikipedia.org/wiki/Cat%C3%A9gorie:Cr%C3%A9ature_fantastique_de_la_mythologie_polyn%C3%A9sienne",
    "aborigen": "https://fr.wikipedia.org/wiki/Cat%C3%A9gorie:Cr%C3%A9ature_fantastique_de_la_mythologie_aborig%C3%A8ne"
}

def save_in_file(worlds):
    with open("res.json", "w") as f:
        json.dump([world.to_dict() for world in worlds], f)

def main():
    worlds = []
    for world, url in worlds_dict.items():
        current_world = World(name=world, url=url)
        monsters = scrape_monsters(url)
        current_world.monsters =    monsters
        worlds.append(current_world)
    for world in worlds:
        print(world)
    save_in_file(worlds)


if __name__ == "__main__":
    main()
